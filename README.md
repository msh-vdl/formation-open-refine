# Formation Découverte d’OpenRefine

## MSH - 10 avril 2024

La formation s’appuie sur le [tutoriel de Mathieu Saby](https://msaby.gitlab.io/tutoriel-openrefine/).

Données de la base géographique [GeoNames](https://www.geonames.org), sous licence [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

Extension VIB-Bits, sous licence [GPL 3](https://www.gnu.org/licenses/gpl-3.0.en.html)

Les autres fichiers sont fournis par Olivier Marlet

[https://msh-vdl.gitpages.huma-num.fr/formation-open-refine](https://msh-vdl.gitpages.huma-num.fr/formation-open-refine)